/*
 * Clase para manejar los eventos de SAX al leer el XML
 */
package ejemplosax;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Linkia
 */
public class Manejador extends DefaultHandler {

    private ArrayList<Libro> misLibros;
    private Libro libroActual;
    private String elementoActual;
//    private Libro[] feo;

    public Manejador() {
        misLibros = new ArrayList<>();
//        feo = new Libro[4];
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Libro")) {
            // Creamos un libro
            libroActual = new Libro();
            // Guardar el atributo
            libroActual.setPublicadoEn(atrbts.getValue(atrbts.getQName(0)));
        } else {
            elementoActual = qName;
        }
        
    }

    @Override
    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Libro")) {
            misLibros.add(libroActual);
        }
    }

    @Override
    public void characters(char[] chars, int i, int i1) throws SAXException {
        
    }

    public ArrayList<Libro> getMisLibros() {
        return misLibros;
    }

}
