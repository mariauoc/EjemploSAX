/*
 * Ejemplo con SAX
 */
package ejemplosax;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Linkia
 */
public class EjemploSAX {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            Manejador m = new Manejador();
            parser.parse(new File("libros.xml"), m);
//            for (int i = 0; i < m.getMisLibros().size(); i++ ) {
//                System.out.println(m.getMisLibros().get(i).getTitulo());// feo[i]
//            }
            for (Libro l : m.getMisLibros()) {
                System.out.println("Datos del libro");
                System.out.println("Titulo: " + l.getTitulo());
                System.out.println("Autor: " + l.getAutor());
                System.out.println("Publicado en: " + l.getPublicadoEn());
                System.out.println("*************************");
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(EjemploSAX.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
