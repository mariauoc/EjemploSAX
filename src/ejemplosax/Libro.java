/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplosax;

/**
 *
 * @author Linkia
 */
public class Libro {

    private String titulo;
    private String autor;
    private String publicadoEn;

    public Libro() {
        titulo = "";
        autor = "";
        publicadoEn = "";
    }

    public Libro(String titulo, String autor, String publicadoEn) {
        this.titulo = titulo;
        this.autor = autor;
        this.publicadoEn = publicadoEn;
    }

    @Override
    public String toString() {
        return "Libro{" + "titulo=" + titulo + ", autor=" + autor + ", publicadoEn=" + publicadoEn + '}';
    }
    
    

    /**
     * Get the value of publicadoEn
     *
     * @return the value of publicadoEn
     */
    public String getPublicadoEn() {
        return publicadoEn;
    }

    /**
     * Set the value of publicadoEn
     *
     * @param publicadoEn new value of publicadoEn
     */
    public void setPublicadoEn(String publicadoEn) {
        this.publicadoEn = publicadoEn;
    }

    /**
     * Get the value of autor
     *
     * @return the value of autor
     */
    public String getAutor() {
        return autor;
    }

    /**
     * Set the value of autor
     *
     * @param autor new value of autor
     */
    public void setAutor(String autor) {
        this.autor = autor;
    }

    /**
     * Get the value of titulo
     *
     * @return the value of titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Set the value of titulo
     *
     * @param titulo new value of titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}
